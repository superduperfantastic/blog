---
layout: post
status: publish
published: true
title: Dog-friendly Bodie {#TruLoveIs}
wordpress_id: 23819
wordpress_url: http://superduperfantastic.com/?p=23819
disqus_id: 23819 http://superduperfantastic.com/?p=23819
thumbnail: http://superduperfantastic.com/wp-content/uploads/2015/09/Bodie-Dog-21-1024x683.jpg
image: http://superduperfantastic.com/wp-content/uploads/2015/09/Bodie-Dog-21-1024x683.jpg
date: '2015-09-23 07:18:14 -0700'
categories:
- California
tags:
- Eastern Sierras
- Bodie
- Mono County
- dog-friendly
comments: true
---
*This post is sponsored by PetSmart&reg; & Wellness&reg; Natural Pet Food and the BlogPaws Professional Pet Blogger Network. I am being compensated for helping spread the word about Wellness TruFood&reg;, but [Super Duper Fantastic] only shares information we feel is relevant to our readers. Wellness Natural Pet Food is not responsible for the content of this article.*
<!--more-->
{:.center}
![TruFood](http://superduperfantastic.com/wp-content/uploads/2015/09/TruFood.jpg)

With over 100 years of combined experience in making natural pet food, Wellness&reg; is committed to nutritional innovation and product quality. Slow-baked in small batches and with 70% more raw protein than typical dry dog food, Wellness TruFood&reg; is a blend of whole-prey protein, grain-free fiber, antioxidant-rich superfoods, probiotics, and kibble. <a href="http://www.petsmart.com/featured-shops/trufood/cat-36-catid-800997" target="_blank" rel="nofollow">Wellness TruFood</a> is available in select Petsmart&reg; locations across the country. 
<p style="text-align:right;"><img src="http://superduperfantastic.com/wp-content/uploads/2015/09/Bodie-Dog-1-copy-1024x683.jpg" alt="Dog-Friendly Bodie" /><br /><img src="http://superduperfantastic.com/wp-content/uploads/2015/09/Petsmart-Wellness-Logos-300x64.jpg" alt="Petsmart-Wellness-Logos" width="300" height="64" /></p>

**#TruLoveIs** feeding your dog the very best food, so they can stay vibrant and happy on their adventures with you. TruFood recipes contain grain free fibers, such as chickpeas, flax seed, and lentils, pumpkin, ginger, turmeric, and live yogurt cultures to help support a healthy digestive system. Powerful superfoods such as beets, blueberries, and cranberries also help maintain a healthy weight and support the immune system. Follow Petsmart on <a href="https://twitter.com/petsmart" rel="nofollow" target="_blank">Twitter</a> to learn more about the benefits of feeding natural, living nutrition, and <a href="http://www.petsmart.com/featured-shops/trufood/cat-36-catid-800997" target="_blank" rel="nofollow">Wellness TruFood</a>.

**#TruLoveIs** planning trips where we can be dog-inclusive! Not only do we get to spend more time with our pup, we also save money on doggie daycare. However, it's more for the first reason than the second because many hotels charge an additional fee for pets, so money isn't really the issue at that point. 

While reading up on <a href="http://superduperfantastic.com/tag/bodie" target="_blank">Bodie State Historic Park</a> earlier in the year, we saw that leashed dogs are welcome so we included our pup in our trip planning!

{:.center}
[Dog-friendly Bodie](http://superduperfantastic.com/wp-content/uploads/2015/09/Bodie-Dog-2-1024x683.jpg)\\
*Fresca enjoying the views near the campground*

We picked out <a href="http://paradiseshoresrvpark.com/" target="_blank" rel="nofollow">Paradise Shores</a>, a dog-friendly campground (doggie shower and play area included!) in nearby Bridgeport. They love dogs and don't charge extra for them!

The elevation must have gotten to Fresca because when we were trying to set up our tent in super windy conditions, she just sat there patiently waiting rather than getting all up in our business. The elevation only bothered me on our second day as we were on our way home.
  
Once we were all set up and finished with traipsing through <a href="http://superduperfantastic.com/see-bodie-california-ghost-town-at-night/23822/">Bodie at night</a>, she crossed "spend a night in a tent" off her bucket list (because of course she would have one)!

{:.center}
![Dog-friendly Bodie](http://superduperfantastic.com/wp-content/uploads/2015/09/Bodie-Dog-21-1024x683.jpg)

Dogs are welcome at Bodie State Historic Park as long as they are on leash and their owners pick up after them. There are a few buildings that they are not allowed to enter, such as the museum and the stamp mill, but that's fewer than a handful. It makes sense since there are artifacts on display and interiors in "arrested decay" being preserved.

Fresca enjoyed the sights and smells and didn't seem turned off by the ghosts. A few wild animals did catch her nose a few times, and she was tempted to run after what was rustling in the shrubs. Thankfully, she was mostly well-behaved and cooperated with us on our photo shoots. 

{:.center}
![Dog-friendly Bodie](http://superduperfantastic.com/wp-content/uploads/2015/09/Bodie-Dog-2-2-1024x683.jpg)  
*Chilling in front of the museum*

{:.center}
![Dog-friendly Bodie](http://superduperfantastic.com/wp-content/uploads/2015/09/Bodie-Dog-2-3-1024x683.jpg)  

**#TruLoveIs** maintaining your pup's routines, even when on the road. We are incredibly blessed that Fresca is flexible in terms of being able to travel with us. She adjusts and adapts to new environments because we take the time to familiarize her with new surroundings. We also try to bring some comforts of home along, like a favorite blanket, toy, and of course, her favorite - food and snacks! 

{:.center}
![Dog-friendly Bodie](https://c1.staticflickr.com/1/699/21036298164_3789ac7d06_b.jpg)

Can't wait for our next adventure together!

{:.center}
![TruFood](http://superduperfantastic.com/wp-content/uploads/2015/09/TruFood.jpg)

*Thanks to Petsmart and Wellness TruFood for sponsoring this post. All opinions expressed here are my own.*
