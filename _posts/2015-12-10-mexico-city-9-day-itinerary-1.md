---
layout: post
status: publish
published: true
title: Mexico City in 9 Days (Part I)
wordpress_id: 24020
wordpress_url: http://superduperfantastic.com/?p=24020
disqus_id: 24020 http://superduperfantastic.com/?p=24020
thumbnail: https://c2.staticflickr.com/6/5802/23430232582_a413969592_q.jpg
image: https://c2.staticflickr.com/6/5723/23375003281_9e31b3f4c7_b.jpg
excerpt: <p>Arrival in Mexico City and our official first full day in Mexico City.  We enjoy our first meals and spend some time learning about Mexico City through its food with a tour with Eat Mexico.</p>
date: '2015-12-10 10:23:16 -0800'
categories:
- Mexico
tags:
- Mexico City
comments: true
---
Mexico City was amazing. So much good food, sights to see, things to do. I don't usually post itineraries or recaps in detail, but I need to get back in the blogging game. It may also be useful for those of you considering a trip to Mexico City. It's a pretty quick flight from the states and a relatively inexpensive trip to enjoy.

{:.center}
![Mexico in 9 Days](http://superduperfantastic.com/wp-content/uploads/2015/12/Mexico-in-9-Days.png)

### Arrival in Mexico City - Our First Meals ###

We arrived at the airport and took an Uber to your Airbnb where we quickly dropped our bags off in our room and were whisked away by our hosts for a quick bite to eat. Delicious Marlin tacos at [El Pescadito Condesa DF](https://www.facebook.com/ElPescaditoCondesaDf){:target="_blank"} were our first meal in Mexico City! Pro tip: Instead of standing in the long line to be seated inside at a table, order at the register for take out and pick up your food next door and enjoy on the bench right outside.

{:.center}
![Mexico City - El Pescadito Condesa](https://c2.staticflickr.com/6/5723/23375003281_9e31b3f4c7_b.jpg)

The rest of the afternoon, we tried finding a working ATM and acquiring SIM cards for our phones. Unfortunately, we were met with no success.

In the evening, our hosts helped us with acquiring SIM cards, but we couldn't quite figure out the ATM card situation. Thankfully our credit card worked because we had zero cash on us, not even a peso when the cute little kids all decked out in their Halloween costumes came trick or treating. They don't trick or treat for candy (which I also had none of), so all I could do was turn them away when they approached us on the street.

With our working phones, we made reservations for a fancy restaurant that we happened to walk by in Polanco. It was recommended to us by the community manager of Yelp Mexico City. We showed up to [Biko](http://www.biko.com.mx/){:target="_blank"} in our street clothes, and while we felt awkward for the first five minutes, we soon ordered the tasting menu and shared a wine pairing.

{:.center}
![Mexico City - Biko](https://c1.staticflickr.com/1/577/23030067343_406d36fc5f_b.jpg)

It was a really big meal.

### Day ONE in Mexico City - Eat Mexico / Mercado Jamaica / Birthday BBQ ###

For our first real day in Mexico City. Time to take advantage of all it has to offer! Ride Ecobicis to the [Eat Mexico](http://eatmexico.com/){:target="_blank"} tour you signed up for (Comidas & Calaveritas at Mercado Jamaica). Show up late because you didn't realize how long it would take to get there. Good thing you got SIM cards the night before, so you can text your guide on your way to the meeting point.

Your tour starts, and once you arrive at Mercado Jamaica, your face stuffing begins. Tlacoyos with huitlacoche (aka corn fungus), tamales, champurrado/atole, quesadillas with squash blossoms, huaraches... All the while, you learn about preparations for Dia de los Muertos. The flowers, altars, paper cut-outs and more.

{:.center}
![Mexico City - Mercado Jamaica](https://c2.staticflickr.com/6/5802/23430232582_a413969592.jpg) ![Mexico City - Mercado Jamaica](https://c2.staticflickr.com/6/5658/23374950341_8875294f8f.jpg)

Have so much food and fun, and hop on the subway back to your Airbnb once it's all over and done!

<u>AFTERNOON:</u> Hang out with your Airbnb hosts and their friends because they're having a birthday BBQ! Eat delicious foods, drink beer, and chit chat with the locals. Take a blurry selfie. It'll be the only photo you have with one of your amazing Airbnb hosts!

{:.center}
![Birthday BBQ](https://c2.staticflickr.com/6/5647/23631292646_e13dc1eb96.jpg)

![Mexico City - Panteon Civil de Dolores](https://c2.staticflickr.com/6/5715/23170407199_50d1049a1f_n.jpg)<u>EVENING:</u> Take a break from the party to check out some Dia de los Muertos festivities! Grab an Uber for [Panteón Civil de Dolores](https://en.wikipedia.org/wiki/Pante%C3%B3n_de_Dolores), the largest cemetery in Mexico.

After telling your Uber that you're absolutely sure you want to be dropped off there, you wander the perimeter of the cemetery. There's nobody in sight, and it's super dark out. You come to the conclusion that it is CLOSED. You got here too late. Festivities ended by 9pm! Time to head back to the Airbnb. Luckily, the party is still bumping!

Now to figure out the ATM situation. Finally give in and call your bank via Skype because there's no way every single ATM in Mexico City does not work with your card. Verify your identity and that you've walked up to over a handful ATMs, trying to withdraw varying amounts in order to test your card. Once cleared, test the card at an ATM because you need cash to pay your tour guide the next day. It FINALLY works! *phew*

Buy beer for the party, using cash because their card reader is broken.

>### Mexico City - REAL TALK ###
>- Use Uber to get around. If you have access to an [Ecobici](https://www.ecobici.df.gob.mx/en){:target="_blank"}, the bikes are great for quick trips.
>- Make sure to notify your banks/credit cards early.
>- For SIM cards, get a smaller card first. Add minutes/data online or at a convenient store later. We used Telcel.
>- Book a food/market tour with [Eat Mexico](http://www.eatmexico.com/){:target="_blank"}. It's a bit more expensive, but worth it!
>- Panteón Civil de Dolores closes by 9pm. Get there in the early evening for Dia de Muertos festivities!
>- Eat at: [El Pescadito Condesa DF](https://www.facebook.com/ElPescaditoCondesaDf){:target="_blank"} and [Biko](http://www.biko.com.mx/){:target="_blank"}.
